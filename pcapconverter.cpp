/*
 * pcapconverter.cpp
 *
 *  Created on: 03-May-2019
 *      Author: xhimanshuz
 */


#include<string>
#include<iostream>
#include<pcap.h>
#include"ouch/ouch.hpp"
#include<boost/endian/conversion.hpp>
#include"ouch/soupbin3.hpp"

using namespace OUCH;

void iEnterOrder(u_char *data)
{
	// Putting binary data to EnterOrderMessage Struct
	auto eom = (reinterpret_cast<OUCH::EnterOrderMessage*>(data));
	printf("\nEnter Order Message\nType: %c \nOrder Token: %s \nOrder Book ID: %d \nSide: %c \nQuantity: %d \nPrice: %d\nTime in Force: %d"
			"\nOpen Close: %d \nClient/Account: %s \nCustomer Info: %s \nExchange Info: %s \nDisplay Quantity: %d \n"
			"Client Category: %d \nOffHours: %d\n", eom->type, eom->id, boost::endian::endian_reverse(eom->symbol), eom->side, boost::endian::endian_reverse(eom->quantity), boost::endian::endian_reverse(eom->price),
			boost::endian::endian_reverse(eom->tif), boost::endian::endian_reverse(eom->openOrClose), eom->clientAccount, eom->customerInfo, eom->exchangeInfo, boost::endian::endian_reverse(eom->display), boost::endian::endian_reverse(eom->clientCategory), boost::endian::endian_reverse(eom->offHours));

}

void iReplaceOrder(u_char *data)
{
	auto eom = (reinterpret_cast<OUCH::ReplaceOrderMessage*>(data));
	printf("\nReplacement Order Message\nType: %c \nExisting Order: %s \nReplacement order Token: %s \nQuantity: %d \nPrice: %d"
				"\nOpen Close: %d \nClient/Account: %s \nCustomer Info: %s \nExchange Info: %s \nDisplay Quantity: %d \n"
				"Client Category: %d\n", eom->type, eom->existingOrderToken, eom->replacementOrderToken, boost::endian::endian_reverse(eom->quantity), boost::endian::endian_reverse(eom->price),
				boost::endian::endian_reverse(eom->openOrClose), eom->clientAccount, eom->customerInfo, eom->exchangeInfo, boost::endian::endian_reverse(eom->display), boost::endian::endian_reverse(eom->clientCategory));
}

void  iCancelOrder(u_char *data)
{
	auto eom = reinterpret_cast<OUCH::CancelOrderMessage*>(data);
	printf("\nCancel Order Message\n"
			"Type: %c\n"
			"Order Token: %s\n"
			"", eom->TYPE, eom->id);
}

void iCancelOrderID(u_char *data)
{
	auto eom = reinterpret_cast<OUCH::CancelByOrderIdMessage*>(data);
	eom->ntoh();
	std::cout<< eom->toString();

}

void sbtLoginAccept(u_char *data)
{

}


void oOrderAccepted(u_char *data)
{
	auto eom = reinterpret_cast<OUCH::OrderAcceptedMessage*>(data);
	eom->ntoh();
	std::cout<< eom->toString();
//	printf("\nOrder Accept Message\n"
//			"Type: %c\n"
//			"Time Stamp: %u\n"
//			"Order Token: %s\n"
//			"Order Book Id: %d\n"
//			"Side: %c\n"
//			"Order ID: %d\n"
//			"Quantity %d\n"
//			"Price: %d\n"
//			"Time in Force: %d\n"
//			"Open Close: %s\n"
//			"Order State: %d\n"
//			"Customer Info: %s\n"
//			"Exchange Info: %s\n"
//			"Pre Trade Quantity: %d\n"
//			"Display Quantity: %d\n"
//			"Client Category: %d\n"
//			"OffHours: %d\n"
//			"", eom->TYPE, boost::endian::endian_reverse(eom->tm), eom->orderToken, boost::endian::endian_reverse(eom->symbol), eom->side,
//			boost::endian::endian_reverse(eom->orderId), boost::endian::endian_reverse(eom->quantity),
//			boost::endian::endian_reverse(eom->price), boost::endian::endian_reverse(eom->tif), boost::endian::endian_reverse(eom->openOrClose),
//			eom->clientAccount,  eom->orderState, eom->customerInfo, eom->exchangeInfo, boost::endian::endian_reverse(eom->perTradeQuantity),
//			boost::endian::endian_reverse(eom->display), eom->clientCategory, eom->offHours);
}

void oOrderRejected(u_char *data)
{
	auto eom = reinterpret_cast<OUCH::OrderRejectedMessage*>(data);
	printf("\nOrderRejectedMessage\n"
			"Type: %c\n"
			"Time Stamp: %u\n"
			"Order Token: %s\n"
			"Reject Code: %d\n"
			"", eom->TYPE, boost::endian::endian_reverse(eom->tm), eom->id, boost::endian::endian_reverse(eom->reason));

}

void oOrderReplaced(u_char *data)
{
	auto eom = reinterpret_cast<OUCH::OrderReplacedMessage*>(data);
	printf("\nOrderReplaced\n"
			"Type: %c\n"
			"Time Stamp: %u\n"
			"Replacement Order Token: %s\n"
			"Previous Order Token: %s\n"
			"Order Book ID: %d\n"
			"Side: %c\n"
			"Order ID: %d\n"
			"Quantity: %d\n"
			"Price: %d\n"
			"Time in force: %d\n"
			"Open Close: %d\n"
			"Client/Account: %s\n"
			"Order State: %d\n"
			"Customer Info: %s\n"
			"Exchange Info: %s\n"
			"Pre Trade Quantity: %d\n"
			"Display Quantity: %d\n"
			"Client Category: %d"
			"", eom->TYPE, eom->tm, eom->replacementOrderToken, eom->previousOrderToken,
			boost::endian::endian_reverse(eom->orderBookId), eom->side, boost::endian::endian_reverse(eom->orderId),
			boost::endian::endian_reverse(eom->quantity), boost::endian::endian_reverse(eom->price),
			boost::endian::endian_reverse(eom->tif), eom->openOrClose, eom->clientAccount,
			eom->orderState, eom->customerInfo, eom->exchangeInfo, boost::endian::endian_reverse(eom->perTradeQuantity),
			boost::endian::endian_reverse(eom->display), boost::endian::endian_reverse(eom->clientCategory));

}

void oOrderCanceled(u_char *data)
{
	auto eom = reinterpret_cast<OUCH::OrderCanceledMessage*>(data);
	printf("\nOrderCanceled\n"
			"Type: %c\n"
			"Time Stamp: %u\n"
			"Order Token: %s\n"
			"Order Book ID: %d\n"
			"Side: %c\n"
			"Order ID: %d\n"
			"Reason: %d\n"
			"", eom->TYPE, eom->tm, eom->id, boost::endian::endian_reverse(eom->symbol),
			eom->side, boost::endian::endian_reverse(eom->orderId),
			boost::endian::endian_reverse(eom->reason));
}

void oOrderExecuted(u_char *data)
{
	auto eom = reinterpret_cast<OUCH::OrderExecutedMessage*>(data);
	printf("\nOrderExecuted\n"
			"Type: %c\n"
			"Time Stamp: %u\n"
			"Order Token: %s\n"
			"Order Book ID: %d\n"
			"Trade Quantity: %d\n"
			"Trade Price: %d\n"
			"Match ID: %d\n"
			"Client Category: %d\n"
			"", eom->type, boost::endian::endian_reverse(eom->tm), eom->orderToken, boost::endian::endian_reverse(eom->orderBookId), boost::endian::endian_reverse(eom->tradedQuantity), boost::endian::endian_reverse(eom->tradedPrice), boost::endian::endian_reverse(*eom->matchId), eom->clientCategory);
}

int main(int argc, char **argv)
{
	int hFlag = 0; // HEARTBEAT ARGUMENET
	if(argc>1 && *argv[1] == 'h')
		hFlag = 1;
	if(hFlag)
		printf("Displaying Heartbeat Packets is Enabled.\n");
	else
		printf("Displaying Heartbeat Packets is Disabled.\n");

//	char *file = "/home/gnu/Downloads/Packet captures/cancel.pcapng"; // Contain only add and replace data
    char* file = "addReplace.pcapng";		// default file

    std::cout<<"\nFile: "<< file<<std::endl;

    char errbuf[PCAP_ERRBUF_SIZE];

    //Open pcap file
    auto *handler = pcap_open_offline(file, errbuf);
    if(handler == NULL)
    {
        std::cerr<< "\nError in Handler!\n";
        return 2;
    }
    // Packet header
    struct  pcap_pkthdr *header;

    const u_char *data;
    u_char *oData;

    u_int packectCount = 0;
    while(int returnValue = pcap_next_ex(handler, &header, &data)>=0)
    {

    	// OUCHBINTCP MESSAGE SEND MY SOUPBINTCP CLIENT
    	switch(data[54])
    	{
    	case 'L':
    	{
    		oData = new u_char[sizeof(OUCH::soupbin3_packet_login_request)];
    		std::copy(data+52, data+(52+sizeof(OUCH::soupbin3_packet_login_request)), oData);
    		auto sb = reinterpret_cast<OUCH::soupbin3_packet_login_request*>(oData);
    		printf("Login Requested - Type: %c, Username: %s, Password: %s, Request Session: %s, Request Sequence Number: %d\n", sb->PacketType, sb->Username, sb->Password, sb->RequestedSession, sb->RequestedSequenceNumber);
    		break;
    	}
    	case 'U':
    	{
    		oData = new u_char[sizeof(OUCH::soupbin3_packet_unseq_data)];
    		std::copy(data+55, data+(55+sizeof(OUCH::soupbin3_packet_unseq_data)), oData);
    		auto sb = reinterpret_cast<OUCH::soupbin3_packet_unseq_data*>(oData);
    		printf("Unsequenced Data Packet - Type: %c, Message: %s\n", sb->PacketType, sb->Message);
    		break;
    	}
    	case 'R':
    		if(!hFlag)
    			break;
    		printf("Heartbeat Packet sent by Client.\n");
    		break;
    	}

        // INBOUND MESSAGE: 52 + 3[OuchBinTCP] = 55
        // 0-54 bytes contain non INBOUND Messages
        switch (data[55])
        {
        case 'O':
//        	// ENTER ORDER MESSAGE: 114 bytes
        	oData = new u_char[sizeof(OUCH::EnterOrderMessage)];
			std::copy(data+55, data+(55+sizeof(OUCH::EnterOrderMessage)), oData);
			iEnterOrder(oData);
			break;
        case 'U':
//          // REPLACE ORDER MESSAGE
        	oData = new u_char[sizeof(OUCH::ReplaceOrderMessage)];
        	std::copy(data+55, data+(55+sizeof(OUCH::ReplaceOrderMessage)), oData);
        	iReplaceOrder(oData);
        	break;
        case 'X':
        	// ORDER CANCEL MESSAGE
        	oData = new u_char[sizeof(OUCH::CancelOrderMessage)];
        	std::copy(data+55, data+(55+sizeof(OUCH::CancelOrderMessage)), oData);
        	iCancelOrder(oData);
        	break;
        case 'Y':
        	// /ORDER CANCEL BY ID MESSAGE
        	oData = new u_char[sizeof(OUCH::CancelByOrderIdMessage)];
        	std::copy(data+55, data+(55+sizeof(OUCH::CancelByOrderIdMessage)), oData);
        	iCancelOrderID(oData);
        	break;
        }

    	//OUCHBINTCP MESSAGE SEND BY SOUPBINTCP SERVER
    	switch(data[56])
    	{
    	case '+':
    	{
    		oData = new u_char[sizeof(OUCH::soupbin3_packet_debug)];
    		auto sb = reinterpret_cast<OUCH::soupbin3_packet_debug*>(oData);
    		printf("Debug Packet - Type: %c, Text: %s", sb->PacketType, sb->Text);
    		break;
    	}
    	case 'A':
    	{
    		oData = new u_char[sizeof(OUCH::soupbin3_packet_login_accepted)];
    		std::copy(data+54, data+(54+sizeof(OUCH::soupbin3_packet_login_accepted)), oData);
    		auto sb = reinterpret_cast<OUCH::soupbin3_packet_login_accepted*>(oData);
    		printf("Login Accepted Packet - Type: %c, Session: %s, Sequence Number: %u\n", sb->PacketType, sb->Session, sb->SequenceNumber);
    		break;
    	}
    	case 'J':
    	{
    		oData = new u_char[sizeof(OUCH::soupbin3_packet_login_rejected)];
    		std::copy(data+54, data+(54+sizeof(OUCH::soupbin3_packet_login_rejected)), oData);
    		auto sb = reinterpret_cast<OUCH::soupbin3_packet_login_rejected*>(oData);
    		printf("Login Rejected Packet - Type: %c, Reject Reason: %s", sb->PacketType, [=]()->std::string {
    			return (sb->RejectReasonCode == 'A')?"Code: A - Not Authorized. There was an invalid username and password combination in the Login Request Message.":"Code: S - Session not available. The Requested Session in the Login Request Packet was either invalid or not available.";
    		});
    		break;
    	}
    	case 'S':
    	{
    		oData = new u_char[sizeof(OUCH::soupbin3_packet_seq_data)];
    		std::copy(data+54, data+(54+sizeof(OUCH::soupbin3_packet_seq_data)), oData);
    		auto sb = reinterpret_cast<OUCH::soupbin3_packet_seq_data*>(oData);
    		printf("Sequence Data Packet - Type: %c, Message: %s", sb->PacketType, sb->Message);
    		break;
    	}
    	case 'H':
    		if(!hFlag)
    			break;
    		printf("Heartbeat Packet sent by a server.\n");
    		break;
    	case 'Z':
    		printf("End of Session Packet.\n");
    		break;
    	}

        // OUTBOUND MESSAGE:  54 + 3[OuchBinTCP] = 57
        //  0-56 bytes contain non OUTBOUND Messages
        switch(data[57])
        {
        case 'A':
        	// ORDER ACCEPTED
        	oData = new u_char[sizeof(OUCH::OrderAcceptedMessage)];
        	std::copy(data+57, data+(57+sizeof(OUCH::OrderAcceptedMessage)), oData);
        	oOrderAccepted(oData);
        	break;
        case 'J':
        	// ORDER REJECTED
        	oData = new u_char[sizeof(OUCH::ReplaceOrderMessage)];
        	std::copy(data+57, data+(57+sizeof(OUCH::OrderRejectedMessage)), oData);
        	oOrderRejected(oData);
        	break;
        case 'U':
        	// ORDER REPLACED
        	oData = new u_char[sizeof(OUCH::OrderReplacedMessage)];
        	std::copy(data+57, data+(57+sizeof(OUCH::OrderReplacedMessage)), oData);
        	oOrderReplaced(oData);
        	break;
        case 'C':
        	// ORDER CANCELED
        	oData = new u_char[sizeof(OUCH::OrderCanceledMessage)];
        	std::copy(data+57, data+(57+sizeof(OUCH::OrderCanceledMessage)), oData);
        	oOrderCanceled(oData);
        	break;
        case 'E':
        	// ORDER EXECUTED
        	oData = new u_char[sizeof(OUCH::OrderExecutedMessage)];
        	std::copy(data+57, data+(57+sizeof(OUCH::OrderExecutedMessage)), oData);
        	oOrderExecuted(oData);
        	break;
        }

//        printf("\n----------------\n");
    }
}
